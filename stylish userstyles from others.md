# Downloading instructions / redirect to proper download

The Stylish/userstyles.org extension is spyware that hijacks hyperlinks. Use the **[Stylus](https://chromewebstore.google.com/detail/stylus/clngdbkpkpeebahjckkjfobafhncgmne?hl=en-GB)** extension instead.

Here's what's handy! This redirection userscript!

* [Redirect userstyles.org to uso.kkx.one (UserStyles.org Archive)](https://greasyfork.org/en/scripts/451486-redirect-userstyles-org-to-uso-kkx-one-userstyles-org-archive)

# Gitlab

* [Gitlab Dark Latte Orange](https://userstyles.org/styles/166958/gitlab-dark-latte-orange)

# Youtube
* [Better Full Dark Theme 2022 Arc](https://userstyles.org/styles/123887/youtube-better-full-dark-theme-2022-arc)
* [Youtube Glass](https://userstyles.org/styles/257857/youtube-glass-modern-glassy-ui-for-youtube)

**Modify Full Dark Arc Theme**
```
/* change the background colour of the sidebar */
ytd-engagement-panel-title-header-renderer, ytd-transcript-footer-renderer[panel], #star-survey.ytd-inline-survey-renderer, ytd-inline-survey-renderer[expanded] #dismissable.ytd-inline-survey-renderer, #dismiss-button.ytd-inline-survey-renderer, #show-hide-button.ytd-live-chat-frame>ytd-toggle-button-renderer.ytd-live-chat-frame, ytd-watch-next-secondary-results-renderer, #alerts.ytd-watch, #pla-shelf.ytd-watch, #info.ytd-watch, #meta.ytd-watch, #ticket-shelf.ytd-watch, #merch-shelf.ytd-watch, #report.ytd-watch, #comments.ytd-watch, #content-separator.ytd-watch, ytd-live-chat-frame[watch-color-update_] #show-hide-button.ytd-live-chat-frame>ytd-toggle-button-renderer.ytd-live-chat-frame, ytd-playlist-panel-video-renderer:nth-child(even), ytd-playlist-panel-renderer[collapsible] .header.ytd-playlist-panel-renderer, ytd-live-chat-frame[watch-color-update] #show-hide-button.ytd-live-chat-frame>ytd-toggle-button-renderer.ytd-live-chat-frame {
    background-color: #121212 !important;
}
```

# Twitter

* [New Twitter Fixes](https://userstyles.org/styles/173922/new-twitter-fixes)
* [Derounding Twitter](https://userstyles.org/styles/144146/de-rounding-twitter-works-with-new-lay-out)
