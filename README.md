# Miscellaenous Settings for Web Browser Extensions

## Contents

* Copy Links
* I don't care about cookies
* Image Picka
* Redirector
* Window Resizer

## Don't Forget

| Extension | Setting to choose |
| ------ | ------ |
|  Image Picka      |    Detect image URLs from links    |
|  Image Picka      |    `#767676` colour    |

## See Also
* [My userscripts](https://gitlab.com/desbest/My-Userscripts)
* [My adblock plus rules](https://gitlab.com/-/snippets/2249226)
* [My scraping rules](https://gitlab.com/desbest/scraping-presets)
